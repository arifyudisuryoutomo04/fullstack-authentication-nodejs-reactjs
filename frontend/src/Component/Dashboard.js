import React, { useState, useEffect } from "react";
import axios from "axios";
import jwt_decode from "jwt-decode";
import { useNavigate } from "react-router-dom";
import Navbar from "./Navbar";

const Dashboard = () => {
  // TODO: TEST API REFRESH TOKEN
  // var token =
  //   "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjYsIm5hbWUiOiJhcmlmIiwiZW1haWwiOiJhcmlmQGdtYWlsLmNvbSIsImlhdCI6MTY1MzgxMDMyMywiZXhwIjoxNjUzODEwMzUzfQ.YfSlx6TD1DYrFJpSZwGemUhOOEPJRKzZABH9JL1mmf0";
  // var decoded = jwt_decode(token);
  // console.log(decoded);
  // ! END TEST API REFRESH TOKEN

  const [name, setName] = useState("");
  const [token, setToken] = useState("");
  const [expire, setExpire] = useState("");
  const [users, setUsers] = useState([]);
  const navigate = useNavigate();

  useEffect(() => {
    refreshToken();
    getUsers();
  }, []);

  const refreshToken = async () => {
    try {
      const y = await axios.get("http://localhost:5000/refresh-token");
      setToken(y.data);
      const x = jwt_decode(y.data);
      setName(x.name);
      setExpire(x.exp);
      // console.log(y);
    } catch (error) {
      if (error.y) {
        navigate.push("/auth/login");
      }
    }
  };

  const axiosJWT = axios.create();

  axiosJWT.interceptors.request.use(
    async (config) => {
      const currentDate = new Date();
      if (expire * 1000 < currentDate.getTime()) {
        const y = await axios.get("http://localhost:5000/refresh-token");
        config.headers.Authorization = `Bearer ${y.data}`;
        setToken(y.data);
        const x = jwt_decode(y.data);
        setName(x.name);
        setExpire(x.exp);
      }
      return config;
    },
    (error) => {
      return Promise.reject(error);
    }
  );

  const getUsers = async () => {
    const y = await axiosJWT.get("http://localhost:5000/users", {
      headers: {
        Authorization: `Bearer ${token}`
      }
    });
    setUsers(y.data);
    // console.log(y.data);
  };

  return (
    <div>
      <Navbar />
      <h1 className="title">Wellcome Back: {name}</h1>
      <button onClick={getUsers} className="button is-info">
        Get Users
      </button>
      <div>
        <table className="table is-striped is-fullwidth">
          <thead>
            <tr>
              <th>No</th>
              <th>Name</th>
              <th>Email</th>
            </tr>
          </thead>
          <tbody>
            {users.map((user, index) => (
              <tr key={user.id}>
                <td>{index + 1}</td>
                <td>{user.name}</td>
                <td>{user.email}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default Dashboard;
