import Users from "../Models/UseerModel.js";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";

export const getUsers = async(req, res) => {
    try {
        const x = await Users.findAll();
        res.json(x);
    } catch (error) {
        console.log(error);
    }
};

export const Register = async(req, res) => {
    const { name, email, password, confPassword } = req.body;
    if (password !== confPassword)
        return res
            .status(400)
            .json({ massage: "Password dan Confirm Password Tidak Cocok" });
    const salt = await bcrypt.genSalt();
    const hashPassword = await bcrypt.hash(password, salt);

    try {
        await Users.create({
            name: name,
            email: email,
            password: hashPassword
        });
        res.json({ message: "Register Berhasil" });
    } catch (error) {
        console.log(error);
    }
};

export const Login = async(req, res) => {
    try {
        const x = await Users.findAll({
            where: {
                email: req.body.email
            }
        });
        const match = await bcrypt.compare(req.body.password, x[0].password);
        if (!match) return res.status(400).json({ message: "Wrong Password" });
        const userId = x[0].id;
        const name = x[0].name;
        const email = x[0].email;

        const accessToken = jwt.sign({ userId, name, email },
            process.env.ACCESS_TOKEN_SECRET, {
                expiresIn: "30s"
            }
        );
        const refreshToken = jwt.sign({ userId, name, email },
            process.env.REFRESH_TOKEN_SECRET, {
                expiresIn: "1d"
            }
        );
        await Users.update({ refresh_token: refreshToken }, {
            where: {
                id: userId
            }
        });
        res.cookie("refreshToken", refreshToken, {
            httpOnly: true,
            maxAge: 24 * 60 * 60 * 1000
        });
        res.json({ accessToken });
    } catch (error) {
        res.status(404).json({ message: "Email Tidak Di Temukan" });
    }
};

export const Logout = async(req, res) => {
    const refreshToken = req.cookies.refreshToken;
    if (!refreshToken) return res.sendStatus(204); // ! No Content
    const x = await Users.findAll({
        where: {
            refresh_token: refreshToken
        }
    });
    if (!x[0]) {
        return res.sendStatus(204); // ! No Content
    }
    const userId = x[0].id;
    await Users.update({ refresh_token: null }, {
        where: {
            id: userId
        }
    });
    res.clearCookie("refreshToken");
    return res.sendStatus(200);
};