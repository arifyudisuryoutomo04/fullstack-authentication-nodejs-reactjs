import Users from "../Models/UseerModel.js";
import jwt from "jsonwebtoken";

export const refreshToken = async(req, res) => {
    try {
        const refreshToken = req.cookies.refreshToken;
        if (!refreshToken) return res.sendStatus(401); // ! unAuthorization
        const x = await Users.findAll({
            where: {
                refresh_token: refreshToken
            }
        });
        if (!x[0]) {
            return res.sendStatus(403); // ! forbiden
        } else {
            jwt.verify(
                refreshToken,
                process.env.REFRESH_TOKEN_SECRET,
                (err, decoded) => {
                    if (err) {
                        return sendStatus(403);
                    } else {
                        const userId = x[0].id;
                        const name = x[0].name;
                        const email = x[0].email;

                        const accessToken = jwt.sign({ userId, name, email },
                            process.env.ACCESS_TOKEN_SECRET, {
                                expiresIn: "30s"
                            }
                        );
                        res.json(accessToken);
                    }
                }
            );
        }
    } catch (error) {
        console.log({ message: "Refresh Token Error", error });
    }
};