import express from "express";
import {
    getUsers,
    Register,
    Login,
    Logout
} from "../Controllers/UserController.js";
import { verifyToken } from "../Middleware/VerifyToken.js";
import { refreshToken } from "../Controllers/refreshToken.js";

const router = express.Router();

router.get("/users", verifyToken, getUsers);
router.post("/users", Register);
router.post("/login", Login);
router.get("/refresh-token", refreshToken);
router.delete("/logout", Logout);

export default router;