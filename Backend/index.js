import express from "express";
import db from "./Config/Database.js";
import router from "./Routes/UserRoute.js";
import dotenv from "dotenv";
import cookieParser from "cookie-parser";
import cors from "cors";
// import Users from "./Models/UseerModel.js";

dotenv.config();

const app = express();

try {
    await db.authenticate();
    console.log("Database Connected");

    // ? works to add a table if you don't have a table named users
    // await Users.sync();
} catch (error) {
    console.log(error);
}

app.use(cors({ credentials: true, origin: "http://localhost:3000" }));

app.use(cookieParser());

app.use(express.json());

app.use(router);

app.listen(5000, () => console.log("Server Runing at Port 5000"));